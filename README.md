# gitlabsos

gitlabsos provides a unified method of gathering info and logs from GitLab
and the system it's running on.

## When you should run the script

If performance is degraded, then the script works best if you run it _while_
you're experiencing the issue.

If you're reproducing a software error (like a 500 error), then the script works
best if you run it just _after_ the issue.

Don't fret if you miss your window of opportunity. Run gitlabsos
anyway and it will still gather a wealth of helpful information for
identifying issues.

## Limitations

- **Works on Omnibus installs and GitLab Docker images**. This means that the script won't collect
    GitLab-related information for k8s chart deployments or source installs.
    Use [our kubesos script](https://gitlab.com/gitlab-com/support/toolbox/kubesos#kubesos) instead (for k8s).
- The resulting archive _could_ end up being large. In an attempt to reduce the
    log and archive size, gitlabsos only grabs the last 10MB of each log file.
    In case you want to provide files to GitLab Support that are larger than our ticket system's attachment limit,
    [use our workaround options](https://about.gitlab.com/support/providing-large-files.html).

## Get started

### Usage

If needed, you can always double-check the available options by running `./gitlabsos.rb --help`.

```text
Usage: gitlabsos.rb [options]
    -o, --output-file FILE           Write gitlabsos report to FILE
        --debug                      Set the log level to debug
        --skip-root-check            Run the script as non-root. Warning: script might fail
        --skip-config                Don't include a sanitized copy of the gitlab.rb configuration file.
        --max-file-size MB           Set the max file size (in megabytes) for any file in the report
    -h, --help                       Prints this help
```

### Run the script

The gitlabsos program is a simple ruby script, and is designed to run "out of
the box" without any further configuration. See below for an example of how
you can execute the script.

```sh
/opt/gitlab/embedded/bin/git clone --recursive https://gitlab.com/gitlab-com/support/toolbox/gitlabsos.git && cd gitlabsos
sudo ./gitlabsos.rb
```

If your GitLab consists of [multiple nodes](https://docs.gitlab.com/ee/administration/reference_architectures/#gitlab-package-omnibus),
running gitlabsos on all nodes is recommended.

#### Execute directly

Running gitlabsos with this method **will not** collect the `gitlab.rb` file.

```sh
curl https://gitlab.com/gitlab-com/support/toolbox/gitlabsos/raw/master/gitlabsos.rb | sudo /opt/gitlab/embedded/bin/ruby
```

#### Execute on a network not connected to the internet

If your GitLab server is blocked from the outside internet, you can copy the contents
of [this file](./gitlabsos.rb) into a file named `gitlabsos.rb` on your GitLab
server and run it with the following:

```sh
sudo /opt/gitlab/embedded/bin/ruby gitlabsos.rb
```

#### Run gitlabsos in a GitLab Docker Container

First capture the gitlabsos report by executing the `gitlabsos` script in your GitLab container

```sh
sudo docker exec -it <gitlab-container-name> bash -c "curl https://gitlab.com/gitlab-com/support/toolbox/gitlabsos/raw/master/gitlabsos.rb | /opt/gitlab/embedded/bin/ruby"
```

Then copy the gitlabsos report outside the Docker Container to attach it to your Support ticket

```sh
sudo docker cp <gitlab-container-name>:gitlabsos.example.com_<timestamp>.tar.gz .
```

### Output

The script will log some output to your terminal that should look something like this:

```text
gitlabhost:/home/user1/gitlabsos# sudo ./gitlabsos.rb
[2019-08-08T19:43:19.552020] INFO -- gitlabsos: Starting gitlabsos report
[2019-08-08T19:43:19.552141] INFO -- gitlabsos: Gathering configuration and system info..
[2019-08-08T19:43:19.554574] INFO -- gitlabsos: Collecting diagnostics. This will probably take a few minutes..
[2019-08-08T19:43:19.585381] WARN -- gitlabsos: command 'getenforce' doesn't exist
[2019-08-08T19:43:19.588019] WARN -- gitlabsos: command 'sestatus' doesn't exist
[2019-08-08T19:44:12.906677] WARN -- gitlabsos: command 'iotop -aoPqt -b -d 1 -n 10' doesn't exist
[2019-08-08T19:44:33.692803] INFO -- gitlabsos: Getting GitLab logs..
[2019-08-08T19:44:33.790908] INFO -- gitlabsos: Getting unicorn worker active/queued stats..
[2019-08-08T19:44:48.811356] INFO -- gitlabsos: Report finished.
/home/user1/gitlabsos/gitlabsos.user1-gitlabomnibus_20190808194319.tar.gz
```

You'll see several INFO and WARN level log entries. As long as you see "Report
finished" at the end, followed by the path to the tar archive, then you can
safely ignore the rest of the output.
